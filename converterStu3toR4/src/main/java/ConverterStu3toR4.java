package org.hspconsortium.tools.converterStu3toR4;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.convertors.*;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.exceptions.FHIRException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Hello world!
 */
public class ConvertStu3toR4 {
    
    public static String INPUT_DIR = "smart-stu3";
    public static String OUTPUT_DIR = "out";

    public static void main(String[] args) throws IOException, FHIRException {

        Log log = LogFactory.getLog(ConvertStu3toR4.class);

        Path outputDirPath = Paths.get(OUTPUT_DIR);
        if(!Files.exists(outputDirPath)){
            Files.createDirectory(outputDirPath);
        }

        PathMatchingResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
        org.springframework.core.io.Resource[] resourceFiles = resourceResolver.getResources(String.format("classpath:%s/*", INPUT_DIR));

        int totalFiles = resourceFiles.length;
        log.info(String.format("Found %s files to be converted.", totalFiles));

        // converting from STU3, FHIR 3.0.1
        FhirContext fhirContextDstu3 = FhirContext.forDstu3();
        IParser jsonParserDstu3 = fhirContextDstu3.newJsonParser();

        // converting to R4, FHIR v3.3.0
        FhirContext fhirContextR4 = FhirContext.forR4();
        IParser jsonParserR4 = fhirContextR4.newJsonParser();

        // Create converter
        VersionConvertorAdvisor40 advisor = new NullVersionConverterAdvisor40();
        VersionConvertor_30_40 converter = new VersionConvertor_30_40();

        int count = 0;
        for (Resource currentFile : resourceFiles) {
            log.info(String.format("Converting file %s of %s", ++count, totalFiles));

            BufferedReader bufferedReader = Files.newBufferedReader(currentFile.getFile().toPath());
            // right now we're assuming that we are just converting bundles
            Bundle bundle = jsonParserDstu3.parseResource(Bundle.class, bufferedReader);
            bufferedReader.close();

            try {
                org.hl7.fhir.r4.model.Bundle bundleR4 = converter.convertBundle(bundle);

                String newFileName = currentFile.getFilename().substring(0, currentFile.getFilename().lastIndexOf("."));
                newFileName += ".json";

                Path outputPath = Paths.get(OUTPUT_DIR, newFileName);

                try (BufferedWriter writer = Files.newBufferedWriter(outputPath, StandardOpenOption.CREATE)) {
                    jsonParserR4.encodeResourceToWriter(bundleR4, writer);
                }
            } catch (Error err){
                log.error(String.format("Error converting file %s.", count), err);
            }
            log.info("Finished" + currentFile.getFile().toPath());
        }
    }
}
